﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheBridge : MonoBehaviour
{
    public gameLogic taskCompleted;

    public Animator anim;
    // Update is called once per frame
    void Update()
    {
        //when the laptop is placed down move down the bridge
        if (taskCompleted.laptopPlaced == true)
        {
            anim.Play("bridge");
            taskCompleted.laptopPlaced = false;
        }
    }
}
