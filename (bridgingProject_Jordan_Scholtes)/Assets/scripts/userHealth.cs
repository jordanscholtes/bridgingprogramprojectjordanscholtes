﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class userHealth : MonoBehaviour
{


    public int health;
    public int numbOfHealth;

    public GameObject collisionEffect;

    public Image[] healthBars;
    public Sprite fullBar;
    public Sprite emptyBar;

    // Update is called once per frame
    void Update()
    {
        //update the health of the user
        for(int i = 0; i < healthBars.Length; i++)
        {

            if(i < health)
            {
                healthBars[i].sprite = fullBar;
            }
            else
            {
                healthBars[i].sprite = emptyBar;
            }

            if (i < numbOfHealth)
            {
                healthBars[i].enabled = true;
            }
            else
            {
                healthBars[i].enabled = false;
            }
        }
        
        //go to the start screen if the user ran out of health
        if(health == 0)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }

    public void OnTriggerEnter(Collider other)
    {

        //make your health go down if you colide with an enemy
        if(other.gameObject.tag == "Enemy")
        {
            health--;
            GameObject impactNow = Instantiate(collisionEffect, this.transform.position, this.transform.rotation);
            
            Destroy(impactNow, 0.5f);
            Destroy(other.gameObject);
        }

        // reset the level if you hit the boss
        if (other.gameObject.tag == "boss")
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        }

        //die if you collide with the water
        if (other.gameObject.tag == "water")
        {
            health = 0;
        }
    }
}
