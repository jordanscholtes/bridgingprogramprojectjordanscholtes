﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyHealth : MonoBehaviour
{
    public GameObject collisionEffect;
    public float health = 50f;
    public bool fireOnce = false;
    public scoreTimerScript scoring;

    //check the health of the enemy and decide if it ran out of health and destroy the enemy
    public void TakeDamage (float amount)
    {
        health -= amount;
        if (health <= 0f && !fireOnce) 
        {
            Death();
            fireOnce = true;
        }
    }


    //the death function for the enemy
    void Death()
    {
        GameObject impactNow = Instantiate(collisionEffect, this.transform.position, this.transform.rotation);
        ++scoring.score;
        Destroy(impactNow, 0.5f);
        Destroy(gameObject, 0.6f);
    }



}
