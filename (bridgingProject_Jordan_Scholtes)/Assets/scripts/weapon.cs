﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weapon : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;
    public float fireRate = 15f;

    public Camera userCam;

    public ParticleSystem weaponFlash;
    public GameObject collisionEffect;

    private float nextShot = 0f;

    // Update is called once per frame
    void Update()
    { 
        //if the shoot button is pressed and there is enough time between your last shot be allowed to fire again
        if (Input.GetKeyDown(KeyCode.Mouse0) && Time.time >= nextShot)
        {
            nextShot = Time.time + 1f / fireRate;
            Shoot();
        }
    }

    //In this function we check the distance for when a bullet can hit and what layers it should avoid hitting
    void Shoot()
    {

        int layerMask = 1 << 9;

        layerMask = ~layerMask;

        weaponFlash.Play();
     
        RaycastHit hit;
        //here we check if we hit an enemy and if an enemy has been hit in the desired distance take health of the enemy
        if (Physics.Raycast(userCam.transform.position, userCam.transform.forward, out hit, range, layerMask))
        {
            Debug.Log(hit.transform.name);
            enemyHealth target = hit.transform.GetComponent<enemyHealth>();
        if(target != null)
            {
                target.TakeDamage(damage);
            }
            GameObject impactNow = Instantiate(collisionEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impactNow, 1f);


        }
    }
}
