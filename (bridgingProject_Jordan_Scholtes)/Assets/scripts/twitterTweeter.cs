﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class twitterTweeter : MonoBehaviour
{

    public float timer;
    public int score;

    string twitterAddress = "https://twitter.com/intent/tweet";

    string tweetLangauges = "en";

    string tweetText1 = "I conquered Jordan's game with a scoring of ";

    string tweetText2 = " and it took me: ";

    string tweetText3 = " seconds!";

    // Start is called before the first frame update
    void Start()
    {
        //grab the current score 
        score = PlayerPrefs.GetInt("score");
        timer = PlayerPrefs.GetFloat("timer");
    }
    
    //this function opens up twitter and places your score in a text box
    public void uploadScore()
    {
        Application.OpenURL(twitterAddress + "?text=" + UnityWebRequest.EscapeURL(tweetText1 + score + tweetText2 + timer + tweetText3) + "&amp;lang=" + UnityWebRequest.EscapeURL(tweetLangauges));
        Application.Quit();
    }
}
