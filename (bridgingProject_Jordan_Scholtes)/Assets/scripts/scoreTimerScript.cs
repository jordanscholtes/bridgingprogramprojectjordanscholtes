﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreTimerScript : MonoBehaviour
{
    public float timer;
    public int score;
    public Text scoreText;
    public Text timerText;

    //grab the current saved info of the score
    void Start()
    {
    score = PlayerPrefs.GetInt("score");
    timer = PlayerPrefs.GetFloat("timer");
    }

    //set the score if the level is completed
    public void SaveScore()
    {
        PlayerPrefs.SetInt("score", score);
        PlayerPrefs.SetFloat("timer", timer);
    }

    //reset the score when the user restarts the game
    public void setStartScore()
    {
        PlayerPrefs.SetInt("score", 0);
        PlayerPrefs.SetFloat("timer", 0);
    }

    //Update the score and timer
    void Update()
    {

        if (scoreText != null && timerText != null)
        {
            timer += Time.deltaTime;
            scoreText.text = "Score: " + score;
            timerText.text = "Timer: " + timer;
        }
    }
}
