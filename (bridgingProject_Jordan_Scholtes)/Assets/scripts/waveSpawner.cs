﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waveSpawner : MonoBehaviour
{
    public Transform[] spawnPoints;
    public float countDown;
    public Transform enemy;


    //this function makes an enemy spawn on one of the spawnPoints placed on the island
    void SpawnEnemy(Transform _enemy)
    {
        Transform sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
        Instantiate(enemy, sp.position, sp.rotation);
    }


    public void Update()
    {
        //spawn an enemy every 3 seconds
        if(countDown <= 0)
        {
            SpawnEnemy(enemy);
            countDown = 3f;

        }

        countDown -= Time.deltaTime;
    }
}
