﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startGame : MonoBehaviour
{

    public GameObject instruction;
    public GameObject backstory;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    //reset the game to level 1
    public void GameStarter()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    //reset the level to the start screen
    public void GameEnder()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    //toggle for the backstory
    public void Backstory()
    {
        instruction.SetActive(false);
        backstory.SetActive(true);
    }

    //toggle for the instruction
    public void Instruction()
    {
        instruction.SetActive(true);
        backstory.SetActive(false);
    }

    //this function ends the game
    public void GameEnding()
    {
        Application.Quit();
    }


}
