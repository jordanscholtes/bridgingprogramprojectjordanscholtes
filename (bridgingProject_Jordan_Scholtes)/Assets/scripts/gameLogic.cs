﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameLogic : MonoBehaviour
{
    
    public GameObject pistolReal;
    public GameObject pistolFake;
    public GameObject pistolImage;

    public GameObject laptopFake;
    public GameObject laptopImage;

    public GameObject laptopPlacedFake;
    public GameObject laptopPlacedReal;

    public GameObject wave1;
    public GameObject wave2;

    public GameObject shotgunFake;
    public GameObject shotgunReal;
    public GameObject shotgunImage;
    public GameObject tabletImage;

    public GameObject textWeaponPickup;

    public bool laptopPlaced = false;
    public bool twoWeapons = false;

    //task 1 where you pick up the pistol from the box
    public void Task1()
    {
        pistolReal.SetActive(true);
        pistolFake.SetActive(false);
        pistolImage.SetActive(true);
    }


    //task 2 where you pickup the laptop from the beach
    public void Task2()
    {
        laptopFake.SetActive(false);
        laptopImage.SetActive(true);
    }

    //task 3 where you place the laptop on the desk and lower the bridge
    public void Task3()
    {
        laptopPlaced = true;
        laptopPlacedFake.SetActive(false);
        laptopPlacedReal.SetActive(true);
        laptopImage.SetActive(false);
        wave1.SetActive(false);
        wave2.SetActive(true);
    }

    //task 4 where you pickup the shotgun and the tablet
    public void Task4()
    {
        shotgunFake.SetActive(false);
        shotgunImage.SetActive(true);
        tabletImage.SetActive(true);
        textWeaponPickup.SetActive(true);

        twoWeapons = true;

    }

    //task 5 where you place the tablet on the wall
    public void Task5()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(2);

    }

    public void Update()
    {
        //check if the user aquired two weapons if yes make the user switch between those if they press the number 1
        if(twoWeapons == true)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                if (pistolReal.activeSelf)
                {
                    pistolReal.SetActive(false);
                    shotgunReal.SetActive(true);
                }
                else
                {
                    pistolReal.SetActive(true);
                    shotgunReal.SetActive(false);
                }
            }
          
        }

    }

}
