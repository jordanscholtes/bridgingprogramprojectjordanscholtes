﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weaponSwitcher : MonoBehaviour
{

    public GameObject pistolReal;
    public GameObject shotgunReal;

    public scoreTimerScript scoreScript;

    public GameObject robot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (pistolReal.activeSelf)
            {
                pistolReal.SetActive(false);
                shotgunReal.SetActive(true);
            }
            else
            {
                pistolReal.SetActive(true);
                shotgunReal.SetActive(false);
            }
        }

        if (!robot)
        {
            scoreScript.SaveScore();
            UnityEngine.SceneManagement.SceneManager.LoadScene(3);
        }

    }

}

